package com.example.server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IActivity {

    @WebMethod
    MyPojo listAllActivities(int patientId);

    @WebMethod
    MyPojo listMedications(String day,int patientId);
}
