package com.example.server;

import org.json.simple.JSONObject;

import javax.jws.WebService;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "com.example.server.IActivity")
public class ActivityProvider implements IActivity, Serializable {

    @Override
    public MyPojo listAllActivities(int patientId) {
        List<String> arrayList = new ArrayList<String>();
        String url;
        java.sql.Connection myConn = null;
        ArrayList<JSONObject> list= new ArrayList<JSONObject>();
        JSONObject object = null;
        try {
            myConn=java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb","root","starwars");
            Statement myStm=myConn.createStatement();
            ResultSet myRes =myStm.executeQuery("select * from activity where activitypatient_id="+patientId);
            String start_time;
            String end_time;
            String name;
            int activitypatient_id;
            int id;

            while (myRes.next())
            {
                id = myRes.getInt("id");
                name = myRes.getString("activity_name");
                start_time = myRes.getString("start_date");
                end_time = myRes.getString("end_date");
                activitypatient_id=myRes.getInt("activitypatient_id");
                Activity act = new Activity(id,name,start_time,end_time,activitypatient_id);
                String s=act.getID()+" "+act.getActivity()+" "+act.getStart()+" "+act.getEnd()+" "+act.getActivitypatient_id();
                arrayList.add(s);
            }
            myConn.close();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }

        MyPojo pojo = new MyPojo();
        pojo.setListSample(arrayList);
        return pojo;
    }

    @Override
    public MyPojo listMedications(String day, int patientId) {
        List<String> arrayList = new ArrayList<String>();
        String url;
        Connection myConn = null;
        try {
            myConn=java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb","root","starwars");
            Statement myStm=myConn.createStatement();
            ResultSet myRes =myStm.executeQuery("select id from medication_plan where start_date = \""+day+"\" and patient_id="+patientId);
            int id = 0;

            while (myRes.next())
            {
                id = myRes.getInt(1);
            }

            String sql = "select * from medication where medication_plan_id="+id;
            ResultSet rez = myStm.executeQuery(sql);
            int dosage;
            String start_time;
            String end_time;
            String name;
            Boolean taken;
            int idd;

            while (rez.next())
            {
                idd = rez.getInt("id");
                dosage = rez.getInt("dosage");
                start_time = rez.getString("start_time");
                end_time = rez.getString("end_time");
                name = rez.getString("name");
                taken = rez.getBoolean("taken");
                Medication med = new Medication(idd,name,dosage,start_time,end_time,taken);
                String s=med.getId()+" "+med.getName()+" "+med.getDosage()+" "+med.getStartTime()+" "+med.getEndTime()+" "+med.getTaken();
                arrayList.add(s);

            }
            System.out.println(arrayList.size());
            myConn.close();

        }
        catch (Exception e) {
            System.out.println(e.toString());
        }

        MyPojo pojo = new MyPojo();
        pojo.setListSample(arrayList);
        return pojo;
    }

}
