package com.example.server;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class MyRules {

    MyRules(){}

    public void checkRules( int  patientId) {

        List<String> arrayList = new ArrayList<String>();
        String url;
        java.sql.Connection myConn = null;
        java.sql.Connection myConn1 = null;
        int idd=1;

        try {
            myConn = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb", "root", "starwars");
            Statement myStm = myConn.createStatement();
            ResultSet myRes = myStm.executeQuery("select * from activity where activitypatient_id=" + patientId);
            String start_time;
            String end_time;
            String name;
            int activitypatient_id;
            int id;

            while (myRes.next()) {
                id = myRes.getInt("id");
                name = myRes.getString("activity_name");
                start_time = myRes.getString("start_date");
                end_time = myRes.getString("end_date");
                activitypatient_id = myRes.getInt("activitypatient_id");
                Activity act = new Activity(id, name, start_time, end_time, activitypatient_id);

                String d1 = act.getStart();
                String d2 = act.getEnd();
                String activity = act.getActivity();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

                Date date1 = simpleDateFormat.parse(d1);
                Date date2 = simpleDateFormat.parse(d2);

                Calendar calendar = new GregorianCalendar();
                calendar.setTime(date1);
                Calendar calendar1 = new GregorianCalendar();
                calendar1.setTime(date2);

                int start = calendar.get(Calendar.HOUR);
                int end = calendar1.get(Calendar.HOUR);

                int startday = calendar.get(Calendar.DAY_OF_MONTH);
                int endday = calendar1.get(Calendar.DAY_OF_MONTH);
                int day = endday - startday;

                int startmin = calendar.get(Calendar.MINUTE);
                int endmin = calendar1.get(Calendar.MINUTE);

                int startm = 60 - start;
                int endm = 60 - end;
                int minutes = startm + endm;

                int hours = Math.abs(end - start);

                String message = null;

                if (activity.equals("Toileting")) {
                    if (hours >= 1 || (minutes > 60)) {
                        message = " # Notification:  " + activity + "  longer than 1 hours !";
                        System.out.println(message);
                        myConn1 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb", "root", "starwars");
                        Statement myStm1 = myConn1.createStatement();
                        int myRes1= myStm1.executeUpdate("insert into notification "+" (id, message)"+"values ("+id+","+" '"+message+"')");
                        myConn1.close();
                        idd++;
                    }
                }

                if (activity.equals("Showering")) {
                    if (hours >= 1 || (minutes > 60)) {
                        message = " # Notification:  " + activity + "  longer than 1 hours !";
                        System.out.println(message);
                        myConn1 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb", "root", "starwars");
                        Statement myStm1 = myConn1.createStatement();
                        int myRes1= myStm1.executeUpdate("insert into notification "+" (id, message)"+"values ("+id+","+" '"+message+"')");
                        myConn1.close();
                        idd++;
                    }
                }

                if (activity.equals("Leaving")) {
                    if (hours >= 12 || day == 1) {
                        message = " # Notification:  " + activity + "  longer than 12 hours !";
                        System.out.println(message);
                        myConn1 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb", "root", "starwars");
                        Statement myStm1 = myConn1.createStatement();
                        int myRes1= myStm1.executeUpdate("insert into notification "+" (id, message)"+"values ("+id+","+" '"+message+"')");
                        myConn1.close();
                        idd++;
                    }
                }

                if(message!=null) {

                    myConn1 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb", "root", "starwars");
                    Statement myStm1 = myConn1.createStatement();
                    int myRes1= myStm1.executeUpdate("update patient "+"set normal="+" '"+1+"' where id="+ patientId);
                    myConn1.close();


                }
            }
            myConn.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
