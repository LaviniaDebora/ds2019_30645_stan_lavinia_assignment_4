package com.example.server;

import javax.jws.WebMethod;
import javax.xml.bind.annotation.*;
import java.util.List;


public class MyPojo {
    private List<String> listActivity;

    public void setListSample(List<String> list){
        listActivity=list;
    }

    public List<String> getListSample(){
        return listActivity;
    }
}
