package com.example.server;

import javax.xml.ws.Endpoint;

public class ServerPublisher {

    public static void main(String[] args) {
        Endpoint myendpoint = Endpoint.create(new ActivityProvider());
        myendpoint.publish("http://127.0.0.1:8070/myactivity");

        MyRules rules = new MyRules();
        rules.checkRules(1);
    }
}

