package com.example.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

public class Medication implements Serializable {

    private static final long serialVersionUID = 1190476516911661470L;
    private Integer id;
    private String name;
    private Integer dosage;
    private String start;
    private String end;
    private Boolean taken;

    public Medication(){
    }

    public Medication(Integer id, String name, Integer dosage, String start, String end, Boolean taken){
        this.id=id;
        this.name=name;
        this.dosage=dosage;
        this.start=start;
        this.end=end;
        this.taken=taken;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getStartTime() {
        return start;
    }

    public void setStartTime(String start) {
        this.start = start;
    }

    public String getEndTime() {
        return end;
    }

    public void setEndTime(String end) {
        this.end = end;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

}
