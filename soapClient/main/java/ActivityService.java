import api.Chart;
import api.DoctorDisplay;

import org.jfree.ui.RefineryUtilities;
import services.SoapService;

import java.text.ParseException;
import java.util.*;

public class ActivityService {
    public static <ds1> void main(String[] args) throws ParseException {

        SoapService soapService = new SoapService();

        HashMap<String, Integer> ds1 = new HashMap<String, Integer>();
        HashMap<String, ds1> ds2 = new HashMap<String, ds1>();
        ArrayList<String> days = new ArrayList<String>();
        ArrayList<String> activities = new ArrayList<String>();

        for (int i = 0; i < soapService.getSoapActivities().size(); i++) {
            String activity = soapService.getSoapActivities().get(i).toString();
            String[] currencies = activity.split(" ");
            activities.add(currencies[1]);
            String[] arrOfStr = currencies[2].split("T");
            days.add(arrOfStr[0]);
        }

        ArrayList<String> myDays = new ArrayList<String>();
        for (int i = 0; i < days.size(); i++) {

            if (!myDays.contains(days.get(i))) {
                myDays.add(days.get(i));
            }
        }

        ArrayList<String> myActs = new ArrayList<String>();

        for (int i = 0; i < activities.size(); i++) {

            if (!myActs.contains(activities.get(i))) {
                myActs.add(activities.get(i));
            }
        }

        for (int m = 0; m < myActs.size(); m++) {
            ds1.put(myActs.get(m), 0);
        }

        for (int m = 0; m < myDays.size(); m++) {
            for (int k = 0; k < myActs.size(); k++) {
                ds2.put(myDays.get(m), (ds1) ds1);
            }
        }


        Map<String,List<String>> multiMap = new HashMap<String,List<String>>();

        List<String> values = new ArrayList<String>();

        String activity0 = soapService.getSoapActivities().get(0).toString();
        String[] currencies0 = activity0.split(" ");

        String[] arrOfStr0 = currencies0[2].split("T");

        for (int i = 0; i < soapService.getSoapActivities().size(); i++) {
            String activity = soapService.getSoapActivities().get(i).toString();
            String[] currencies = activity.split(" ");

            String[] arrOfStr = currencies[2].split("T");

            if(arrOfStr0[0].equals(arrOfStr[0])){
                values.add(currencies[1]);
            }

            if(!arrOfStr0[0].equals(arrOfStr[0])){
                multiMap.put(arrOfStr0[0],values);
                arrOfStr0[0]=arrOfStr[0];
                values=new ArrayList<String>();
                values.add(currencies[1]);
            }
        }

        HashMap<String,HashMap<String,Integer>> myhash= new HashMap<String,HashMap<String,Integer>>();

        int index=0;
            for(int j=0;j<multiMap.size();j++){
                List<String> va = new ArrayList<String>();
                va=multiMap.get(myDays.get(index));
                List<String> l = new ArrayList<String>();
                l = new ArrayList<String>(new LinkedHashSet<String>(va));
                HashMap<String,Integer> h=new HashMap<String, Integer>();
                for(int i=0;i<l.size();i++){
                    h.put(l.get(i),countOccurrences(va, va.size(), l.get(i)));
                }
                myhash.put(myDays.get(j),h);
        }
            DoctorDisplay doctorDisplay = new DoctorDisplay();
            doctorDisplay.setFrame();

          Chart chart = new Chart("Activities CHART","Activities Statistics",myhash,myDays, myActs);
          chart.pack( );
          RefineryUtilities.centerFrameOnScreen( chart );
         chart.setVisible( true );
        }

    static int countOccurrences(List<String> arr, int n, String x)
    {
        int res = 0;
        for (int i=0; i<n; i++)
            if (x.equals(arr.get(i)))
                res++;
        return res;
    }

}



