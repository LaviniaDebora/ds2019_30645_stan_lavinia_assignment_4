package api;

import data.Medication;
import services.SoapService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class DoctorDisplay  {

     public DoctorDisplay(){
    }

    public void setFrame() {

        final SoapService soapService=new SoapService();

        JFrame frame = new JFrame("Doctor Display");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(790,500);
        frame.setVisible(true);

        Object[] columns = {"Name","Dosage","Start","End", "Taken"};
        JTable table = new JTable(){

            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
        };

        DefaultTableModel model = new DefaultTableModel(){
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0: return String.class;
                    case 1: return String.class;
                    case 2: return String.class;
                    case 3: return String.class;
                    case 4: return Boolean.class;
                    default:return String.class;
                }
            }
        };

        model.setColumnIdentifiers(columns);
        table.setModel(model);
        table.setForeground(Color.black);
        Font font = new Font("",1,18);
        table.setFont(font);
        table.setRowHeight(30);

        JButton btnAdd = new JButton("View");
        btnAdd.setBounds(300, 220, 150, 30);

         JTextField t = new JTextField(40);
        t.setBounds(300, 420, 150, 30);

        JScrollPane pane = new JScrollPane(table);
        pane.setBounds(0, 0, 700, 200);

        JPanel panel = new JPanel(new FlowLayout());
        panel.add(t);
        panel.add(btnAdd);
        frame.setLayout(new FlowLayout());
        frame.add(pane);
        frame.add(panel);
        frame.pack();

        Object[] row = new Object[5];

        btnAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String s = t.getText();
                List med = soapService.getSoapMedications(s);

                for(int i = 0;i<med.size();i++) {
                    String[] currencies = med.get(i).toString().split(" ");
                    row[0] = currencies[1];
                    row[1] = currencies[2];
                    row[2] = currencies[3];
                    row[3] = currencies[4];
                    row[4] = currencies[5];
                    model.addRow(row);
                }
                }
        });

        TableDisplay buttonColumn = new TableDisplay(table, 4);

        frame.setSize(760,567);
        frame.setVisible(true);


    }





}
