package api;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

import java.util.ArrayList;
import java.util.HashMap;

public class Chart extends ApplicationFrame {

    public  HashMap<String, HashMap<String,Integer>> myhash;
    public ArrayList<String> days;
    public ArrayList<String> myact;

    public Chart(String chartTitle, String applicationTitle,HashMap<String, HashMap<String,Integer>> myhash, ArrayList<String> days, ArrayList<String> myact) {
        super( applicationTitle );
        this.myhash=myhash;
        this.days=days;
        this.myact=myact;
        JFreeChart barChart = ChartFactory.createBarChart(
                chartTitle,
                "Activities",
                "Number of Activities",
                createDataset(myhash,days, myact),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel( barChart );
        chartPanel.setPreferredSize(new java.awt.Dimension( 760 , 567 ) );
        setContentPane( chartPanel );
    }

    private CategoryDataset createDataset(HashMap<String, HashMap<String,Integer>> myhash, ArrayList<String> days, ArrayList<String> myact ) {

        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset( );
        int j=2;
        for(int i=0;i<myhash.size();i++){
            HashMap<String,Integer> m = new HashMap<String, Integer>();
            m=myhash.get(days.get(i));
            Integer d=0;
            for(int k=0;k<myact.size();k++){
                d=m.get(myact.get(k));
                dataset.addValue(d, days.get(i), myact.get(k));
            }
            j++;
        }
        return dataset;
    }
}
