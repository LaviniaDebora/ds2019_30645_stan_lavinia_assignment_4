package data;

import data.MyPojo;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public MyPojo createMyPojo() {
        return new MyPojo();
    }

}
