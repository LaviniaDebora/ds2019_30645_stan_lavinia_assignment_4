package data;

import org.json.simple.JSONObject;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

public class Activity implements Serializable {

    final long serialVersionUID = 2290476516911661470L;

    private Integer id;
    private String activity;
    private String start;
    private String end;
    private Integer activitypatient_id;

    Activity(Integer id, String activity, String start, String end, Integer activitypatient_id){
        this.id=id;
        this.activity=activity;
        this.start=start;
        this.end=end;
        this.activitypatient_id=activitypatient_id;
    }

    Activity(){}

    public JSONObject activityToJsonObject(){
        JSONObject json = new JSONObject();
        json.put("id",this.id);
        json.put("activity", this.activity);
        json.put("start",this.start);
        json.put("end",this.end);
        json.put("activitypatient_id",this.activitypatient_id);
        return json;
    }

    public int getID(){
        return this.id;
    }

    public String getActivity(){
        return this.activity;
    }

    public String getStart(){
        return this.start;
    }

    public String getEnd(){
        return this.end;
    }

    public Integer getActivitypatient_id(){
        return this.activitypatient_id;
    }

}