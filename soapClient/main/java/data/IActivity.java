package data;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;


@WebService(name = "IActivity", targetNamespace = "http://server.example.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IActivity {

    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://server.example.com/IActivity/listAllActivitiesRequest", output = "http://server.example.com/IActivity/listAllActivitiesResponse")
    public MyPojo listAllActivities(
        @WebParam(name = "arg0", partName = "arg0")
        int arg0);

    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://server.example.com/IActivity/listMedicationsRequest", output = "http://server.example.com/IActivity/listMedicationsResponse")
    public MyPojo listMedications(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        int arg1);

}
