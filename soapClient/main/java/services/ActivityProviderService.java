package services;

import data.IActivity;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "ActivityProviderService", targetNamespace = "http://server.example.com/", wsdlLocation = "http://127.0.0.1:8070/myactivity?wsdl")
public class ActivityProviderService
    extends Service
{

    private final static URL ACTIVITYPROVIDERSERVICE_WSDL_LOCATION;
    private final static WebServiceException ACTIVITYPROVIDERSERVICE_EXCEPTION;
    private final static QName ACTIVITYPROVIDERSERVICE_QNAME = new QName("http://server.example.com/", "ActivityProviderService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://127.0.0.1:8070/myactivity?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ACTIVITYPROVIDERSERVICE_WSDL_LOCATION = url;
        ACTIVITYPROVIDERSERVICE_EXCEPTION = e;
    }

    public ActivityProviderService() {
        super(__getWsdlLocation(), ACTIVITYPROVIDERSERVICE_QNAME);
    }

    public ActivityProviderService(WebServiceFeature... features) {
        super(__getWsdlLocation(), ACTIVITYPROVIDERSERVICE_QNAME, features);
    }

    public ActivityProviderService(URL wsdlLocation) {
        super(wsdlLocation, ACTIVITYPROVIDERSERVICE_QNAME);
    }

    public ActivityProviderService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ACTIVITYPROVIDERSERVICE_QNAME, features);
    }

    public ActivityProviderService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ActivityProviderService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    @WebEndpoint(name = "ActivityProviderPort")
    public IActivity getActivityProviderPort() {
        return super.getPort(new QName("http://server.example.com/", "ActivityProviderPort"), IActivity.class);
    }

    @WebEndpoint(name = "ActivityProviderPort")
    public IActivity getActivityProviderPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://server.example.com/", "ActivityProviderPort"), IActivity.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ACTIVITYPROVIDERSERVICE_EXCEPTION!= null) {
            throw ACTIVITYPROVIDERSERVICE_EXCEPTION;
        }
        return ACTIVITYPROVIDERSERVICE_WSDL_LOCATION;
    }

}
