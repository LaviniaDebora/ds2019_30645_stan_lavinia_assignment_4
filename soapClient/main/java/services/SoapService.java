package services;

import data.IActivity;
import data.Medication;
import data.MyPojo;

import java.util.List;

public class SoapService {

    public SoapService(){

    }

    public List getSoapActivities(){
        ActivityProviderService service = new ActivityProviderService();
        IActivity calc = service.getActivityProviderPort();

        MyPojo activityPojo = new MyPojo();
        activityPojo=calc.listAllActivities(1);
        List activities = activityPojo.getListSample();

        return activities;
    }

    public List<Medication> getSoapMedications(String day){
        ActivityProviderService service = new ActivityProviderService();
        IActivity calc = service.getActivityProviderPort();

        MyPojo medicationPojo = new MyPojo();
        medicationPojo=calc.listMedications(day,1);
        List medications = medicationPojo.getListSample();

        return medications;
    }
}
